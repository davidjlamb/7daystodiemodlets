﻿// Decompiled with JetBrains decompiler
// Type: ObjectiveTime
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 121D1E4A-719C-47F4-9C22-4FA4FC9D63B1
// Assembly location: E:\SteamLibrary\steamapps\common\7 Days To Die\7DaysToDie_Data\Managed\Assembly-CSharp.dll

using System.IO;
using UnityEngine;

public class ObjectiveDefend : BaseObjective
{
    private float currentTime;
    public static string PropTime = "time";
    private int dayLengthInSeconds;

    public override BaseObjective.ObjectiveValueTypes ObjectiveValueType => BaseObjective.ObjectiveValueTypes.Time;

    public override bool UpdateUI => true;

    public override string StatusText
    {
        get
        {
            if ((double)this.currentTime > 0.0)
                return XUiM_PlayerBuffs.GetTimeString(this.currentTime);
            if (this.Optional)
            {
                this.ObjectiveState = BaseObjective.ObjectiveStates.Failed;
                return Localization.Get("failed");
            }
            this.ObjectiveState = BaseObjective.ObjectiveStates.Complete;
            return Localization.Get("completed");
        }
    }

    public override void SetupObjective()
    {
        this.keyword = Localization.Get("ObjectiveDefend_keyword");
        this.dayLengthInSeconds = GamePrefs.GetInt(EnumGamePrefs.DayNightLength) * 60;
        if (this.CurrentValue == (byte)1)
            return;
        if (this.Value.EqualsCaseInsensitive("day"))
            this.currentTime = (float)this.dayLengthInSeconds;
        else
            this.currentTime = StringParsers.ParseFloat(this.Value);
    }

    public override void SetupDisplay() => this.Description = string.Format("{0}:", (object)this.keyword);

    public override void AddHooks() => QuestEventManager.Current.AddObjectiveToBeUpdated((BaseObjective)this);

    public override void RemoveHooks() => QuestEventManager.Current.RemoveObjectiveToBeUpdated((BaseObjective)this);

    public override void Refresh()
    {
        this.SetupDisplay();
        if (this.Optional)
            this.Complete = (double)this.currentTime > 0.0;
        else
            this.Complete = (double)this.currentTime <= 0.0;
        if (!this.Complete)
            return;
        this.OwnerQuest.CheckForCompletion();
    }

    public override void Read(BinaryReader _br)
    {
        this.currentTime = (float)_br.ReadUInt16();
        this.currentValue = (byte)1;
    }

    public override void Write(BinaryWriter _bw) => _bw.Write((ushort)this.currentTime);

    protected override void CopyValues(BaseObjective objective)
    {
        base.CopyValues(objective);
        ((ObjectiveDefend)objective).currentTime = this.currentTime;
    }

    public override BaseObjective Clone()
    {
        ObjectiveTime objective = new ObjectiveTime();
        this.CopyValues((BaseObjective)objective);
        return (BaseObjective)objective;
    }

    public override void Update(float updateTime)
    {
        this.currentTime -= updateTime;
        if ((double)this.currentTime >= 0.0)
            return;
        this.Refresh();
        this.HandleRemoveHooks();
    }

    public override void HandleFailed()
    {
        this.currentTime = 0.0f;
        this.Complete = false;
    }

    private static string FormatWorldTimeString(int duration)
    {
        int num1 = duration / 24000;
        duration -= num1 * 24000;
        if (num1 > 0)
            return duration >= 23000 ? string.Format("{0}.{1} game day(s)", (object)num1, (object)9) : string.Format("{0}.{1} game day(s)", (object)num1, (object)(int)Mathf.Floor((float)((double)duration / 24000.0 * 10.0 + 0.5)));
        int num2 = duration / 1000;
        duration -= num2 * 1000;
        return duration >= 900 ? string.Format("{0}.{1} game hour(s)", (object)num2, (object)9) : string.Format("{0}.{1} game hour(s)", (object)num2, (object)(int)Mathf.Floor((float)((double)duration / 1000.0 * 10.0 + 0.5)));
    }

    private string FormatWorldTimeTempString(int duration)
    {
        int num1 = duration / this.dayLengthInSeconds;
        duration -= num1 * this.dayLengthInSeconds;
        if (num1 > 0)
            return duration >= this.dayLengthInSeconds ? string.Format("{0}.{1} game day(s)", (object)num1, (object)9) : string.Format("{0}.{1} game day(s)", (object)num1, (object)(int)Mathf.Floor((float)((double)(duration / this.dayLengthInSeconds) * 10.0 + 0.5)));
        int num2 = duration / 1000;
        duration -= num2 * 1000;
        return duration >= 900 ? string.Format("{0}.{1} game hour(s)", (object)num2, (object)9) : string.Format("{0}.{1} game hour(s)", (object)num2, (object)(int)Mathf.Floor((float)((double)duration / 1000.0 * 10.0 + 0.5)));
    }

    public override void ParseProperties(DynamicProperties properties)
    {
        base.ParseProperties(properties);
        if (!properties.Values.ContainsKey(ObjectiveTime.PropTime))
            return;
        this.Value = properties.Values[ObjectiveTime.PropTime];
    }
}
