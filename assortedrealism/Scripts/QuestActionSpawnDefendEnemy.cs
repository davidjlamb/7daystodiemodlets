﻿// Decompiled with JetBrains decompiler
// Type: QuestActionSpawnGSEnemy
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 121D1E4A-719C-47F4-9C22-4FA4FC9D63B1
// Assembly location: E:\SteamLibrary\steamapps\common\7 Days To Die\7DaysToDie_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

public class QuestActionSpawnDefendEnemy : BaseQuestAction {
    public int count = 1;

    public override void PerformAction(Quest ownerQuest) {
        if (!GameStats.GetBool(EnumGameStats.EnemySpawnMode))
            return;
        this.HandleSpawnEnemies(ownerQuest);
    }

    public void HandleSpawnEnemies(Quest ownerQuest) {
        if (this.Value != null && this.Value != "" && !int.TryParse(this.Value, out this.count) && this.Value.Contains("-")) {
            string[] strArray = this.Value.Split('-');
            int int32_1 = Convert.ToInt32(strArray[0]);
            int int32_2 = Convert.ToInt32(strArray[1]);
            this.count = GameManager.Instance.World.GetGameRandom().RandomRange(int32_1, int32_2);
        }
        GameManager.Instance.StartCoroutine(this.SpawnEnemies(ownerQuest));
    }

    private IEnumerator SpawnEnemies(Quest ownerQuest) {
        QuestActionSpawnDefendEnemy actionSpawnDefendEnemy = this;
        EntityPlayerLocal player = ownerQuest.OwnerJournal.OwnerPlayer;
        int lastClassId = 0;
        int i;
        if (SingletonMonoBehaviour<ConnectionManager>.Instance.IsServer) {
            World world = GameManager.Instance.World;
            GameStageDefinition def = GameStageDefinition.GetGameStage(actionSpawnDefendEnemy.ID);
            for (i = 0; i < actionSpawnDefendEnemy.count; ++i) {
                yield return (object)new WaitForSeconds(0.5f);
                int randomFromGroup = EntityGroups.GetRandomFromGroup(def.GetStage(player.PartyGameStage).GetSpawnGroup(0).groupName, ref lastClassId);
                if (randomFromGroup != 0)
                    QuestActionSpawnGSEnemy.SpawnQuestEntity(randomFromGroup, -1, (EntityPlayer)player);
            }
            def = (GameStageDefinition)null;
        }
        else {
            for (i = 0; i < actionSpawnDefendEnemy.count; ++i) {
                yield return (object)new WaitForSeconds(0.5f);
                SingletonMonoBehaviour<ConnectionManager>.Instance.SendToServer((NetPackage)NetPackageManager.GetPackage<NetPackageQuestDefendEntitySpawn>().Setup(actionSpawnDefendEnemy.ID, player.entityId));
            }
        }
    }

    public static void SpawnQuestEntity(int spawnedEntityID, int entityIDQuestHolder, EntityPlayer player = null, int retries = 20) {
        if (retries < 0)
            return;
        World world = GameManager.Instance.World;
        if ((UnityEngine.Object)player == (UnityEngine.Object)null)
            player = world.GetEntity(entityIDQuestHolder) as EntityPlayer;
        Vector3 vector3 = new Vector3((float)((double)world.GetGameRandom().RandomFloat * 2.0 - 1.0), 0.0f, (float)((double)world.GetGameRandom().RandomFloat * 2.0 - 1.0));
        vector3.Normalize();
        float num = (float)((double)world.GetGameRandom().RandomFloat * 12.0 + 12.0);
        Vector3 _transformPos = player.position + vector3 * num;
        if (world.ChunkClusters[0].ChunkProvider.GetDynamicPrefabDecorator().GetPrefabFromWorldPosInsideWithOffset((int)_transformPos.x, (int)_transformPos.z, 5) != null) {
            SpawnQuestEntity(spawnedEntityID, entityIDQuestHolder, player, retries--);
            return;
        }
        float height = (float)GameManager.Instance.World.GetHeight((int)_transformPos.x, (int)_transformPos.z);
        float terrainHeight = (float)GameManager.Instance.World.GetTerrainHeight((int)_transformPos.x, (int)_transformPos.z);
        _transformPos.y = (float)(((double)height + (double)terrainHeight) / 2.0 + 1.5);
        Vector3 _rotation = new Vector3(0.0f, player.transform.eulerAngles.y + 180f, 0.0f);
        Entity entity = EntityFactory.CreateEntity(spawnedEntityID, _transformPos, _rotation);
        entity.SetSpawnerSource(EnumSpawnerSource.Dynamic);
        GameManager.Instance.World.SpawnEntityInWorld(entity);
        (entity as EntityAlive).SetAttackTarget((EntityAlive)player, 300);
    }

    public override BaseQuestAction Clone() {
        QuestActionSpawnGSEnemy action = new QuestActionSpawnGSEnemy();
        this.CopyValues((BaseQuestAction)action);
        return (BaseQuestAction)action;
    }
}
