﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using UnityEngine;

public class ObjectiveDefendOld : BaseObjective {
    private Vector3 position;
    private int neededKillCount;
    private int maxDestroyedBlocks;
    private int destroyedBlocks;

    public override BaseObjective.ObjectiveValueTypes ObjectiveValueType => BaseObjective.ObjectiveValueTypes.Number;

    public override void SetupObjective() {
        this.maxDestroyedBlocks = Convert.ToInt32(this.Value);
        QuestActionSpawnDefendEnemy _action = this.OwnerQuest.Actions.Find(questAction => questAction.Phase == this.Phase && questAction is QuestActionSpawnDefendEnemy) as QuestActionSpawnDefendEnemy;
        if (_action != null)
            this.neededKillCount = _action.count;
    }

    public override void SetupDisplay() {
        this.keyword = Localization.Get("ObjectiveDefend_keyword");
        this.Description = string.Format(this.keyword);
        this.StatusText = string.Format("{0}/{1}", (object)this.CurrentValue, (object)this.neededKillCount);
    }

    public override bool UpdateUI => this.ObjectiveState != BaseObjective.ObjectiveStates.Failed;

    public override void ParseProperties(DynamicProperties properties) {
        base.ParseProperties(properties);
        if (!properties.Values.ContainsKey("MaxDestroyedBlocks"))
            return;
        this.maxDestroyedBlocks = int.Parse(properties.Values["MaxDestroyedBlocks"]);
    }

    public override void AddHooks() {
        this.GetPosition();
        Vector3 pos1 = Vector3.zero;
        Vector3 pos2 = Vector3.zero;
        this.OwnerQuest.GetPositionData(out pos1, Quest.PositionDataTypes.POIPosition);
        this.OwnerQuest.GetPositionData(out pos2, Quest.PositionDataTypes.POISize);
        QuestEventManager.Current.BlockDestroy += new QuestEvent_BlockDestroyEvent(this.Current_BlockDestroy);
        QuestEventManager.Current.ZombieKill += new QuestEvent_NPCKilled(this.Current_ZombieKill);
        QuestEventManager.Current.SubscribeToUpdateEvent(this.OwnerQuest.OwnerJournal.OwnerPlayer.entityId, pos1);
    }

    private void Current_ZombieKill(string name) {
        ++this.CurrentValue;
        this.Refresh();
    }

    private void Current_BlockDestroy(Block block, Vector3i blockPos) {
        if (this.Complete)
            return;
        Rect location = this.OwnerQuest.GetLocationRect();
        if (location.x <= blockPos.x && location.y <= blockPos.z && location.width >= blockPos.x && location.height >= blockPos.z) {
            ++this.destroyedBlocks;
            if (this.destroyedBlocks >= this.maxDestroyedBlocks)
                this.OwnerQuest.MarkFailed();
            this.Refresh();
        }
    }

    private Vector3 GetPosition()
    {
        if (this.OwnerQuest.GetPositionData(out this.position, Quest.PositionDataTypes.POIPosition))
            this.OwnerQuest.Position = this.position;
        return Vector3.zero;
    }

    public override void Refresh() {
        if (this.Complete)
            return;
        this.Complete = (int)this.CurrentValue >= this.neededKillCount;
        if (!this.Complete)
            return;
        this.OwnerQuest.CheckForCompletion();
    }
}
