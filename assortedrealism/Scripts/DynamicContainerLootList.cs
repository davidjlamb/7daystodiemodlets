﻿using HarmonyLib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

static class DynamicContainerLootList {
    private static List<DynamicContainer> containers = new List<DynamicContainer>();

    public static IEnumerator CreateLists(XmlFile _xmlFile) {
        XmlElement documentElement = _xmlFile.XmlDoc.DocumentElement;
        if (documentElement.ChildNodes.Count == 0)
            throw new Exception("No element <containers> found!");
        foreach (XmlNode childNode in documentElement.ChildNodes) {
            if (childNode.NodeType == XmlNodeType.Element && childNode.Name.Equals("container")) {
                XmlElement xmlElement = (XmlElement)childNode;
                ParseContainer(xmlElement);
            }
        }
        yield return (object)null;
    }

    public static void LoadContainers() {
        XmlFile xmlFile = new XmlFile(GameIO.GetGameDir("Mods/AssortedRealism/Config"), "dynamiccontainerloot");
        XmlElement documentElement = xmlFile.XmlDoc.DocumentElement;
        if (documentElement.ChildNodes.Count == 0)
            throw new Exception("No element <containers> found!");
        foreach (XmlNode childNode in documentElement.ChildNodes) {
            if (childNode.NodeType == XmlNodeType.Element && childNode.Name.Equals("container")) {
                XmlElement xmlElement = (XmlElement)childNode;
                ParseContainer(xmlElement);
            }
        }
    }

    public static void Cleanup() {
        containers.Clear();
    }

    private static void ParseContainer(XmlElement elementBlock) {
        DynamicContainer container = new DynamicContainer();
        container.name = elementBlock.GetAttribute("name");
        if (elementBlock.HasAttribute("defaultlootcontainer"))
            container.defaultLootList = elementBlock.GetAttribute("defaultlootcontainer");
        foreach (XmlNode childNode in elementBlock.ChildNodes) {
            if (childNode.NodeType == XmlNodeType.Element && childNode.Name.Equals("list")) {
                XmlElement xmlElement = (XmlElement)childNode;
                DynamicContainerEntry entry = new DynamicContainerEntry();
                if (xmlElement.HasAttribute("poi"))
                    entry.poi = xmlElement.GetAttribute("poi");
                if (xmlElement.HasAttribute("biome"))
                    entry.biome = xmlElement.GetAttribute("biome");
                if (xmlElement.HasAttribute("lootcontainer"))
                    entry.lootList = xmlElement.GetAttribute("lootcontainer");
                container.items.Add(entry);
            }
        }
        containers.Add(container);
    }

    public static string GetLootList(string containerName, string poiName, string biomeName, string defaultLootList) {
        if (containers.Count == 0)
            LoadContainers();
        string retVal = defaultLootList;
        DynamicContainer container = containers.Find(c => c.name == containerName);
        if (container != null) {
            retVal = container.defaultLootList ?? defaultLootList;
            DynamicContainerEntry entry = container.items.Find(c => (c.poi == poiName && c.biome == biomeName) || (c.poi == poiName && c.biome == "") || c.poi == "" && c.biome == biomeName);
            if (entry != null)
                retVal = entry.lootList ?? container.defaultLootList ?? defaultLootList;
        }
        if (retVal.Contains(",")) {
            string[] retArray = retVal.Split(',');
            Random random = new Random();
            int index = random.Next(retArray.Length);
            retVal = retArray[index];
        }
        return retVal;
    }

    public class DynamicContainer
    {
        public string name;
        public string defaultLootList;
        public readonly List<DynamicContainerEntry> items = new List<DynamicContainerEntry>();
    }

    public class DynamicContainerEntry
    {
        public string poi;
        public string biome;
        public string lootList;
    }
}