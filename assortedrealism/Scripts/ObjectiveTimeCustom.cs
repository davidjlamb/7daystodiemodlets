﻿// Decompiled with JetBrains decompiler
// Type: ObjectiveTime
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 121D1E4A-719C-47F4-9C22-4FA4FC9D63B1
// Assembly location: E:\SteamLibrary\steamapps\common\7 Days To Die\7DaysToDie_Data\Managed\Assembly-CSharp.dll

using System.IO;
using UnityEngine;

public class ObjectiveTimeCustom : ObjectiveTime {
    private string descriptionKey = string.Empty;
    public override void SetupObjective() {
        base.SetupObjective();
        this.keyword = descriptionKey ?? Localization.Get("ObjectiveTime_keyword");
    }

    public override void ParseProperties(DynamicProperties properties) {
        base.ParseProperties(properties);
        if (!properties.Values.ContainsKey("description_key"))
            return;
        this.descriptionKey = properties.Values["description_key"];
    }
}
