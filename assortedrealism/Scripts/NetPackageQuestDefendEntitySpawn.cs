﻿public class NetPackageQuestDefendEntitySpawn : NetPackageQuestEntitySpawn {
    private static int lastClassId = -1;

    public override void ProcessPackage(World _world, GameManager _callbacks) {
        if (_world == null)
            return;
        if (this.entityType == -1)
        {
            EntityPlayer entity = GameManager.Instance.World.GetEntity(this.entityIDQuestHolder) as EntityPlayer;
            this.entityType = EntityGroups.GetRandomFromGroup(GameStageDefinition.GetGameStage(this.gamestageGroup).GetStage(entity.PartyGameStage).GetSpawnGroup(0).groupName, ref NetPackageQuestDefendEntitySpawn.lastClassId);
        }
        QuestActionSpawnDefendEnemy.SpawnQuestEntity(this.entityType, this.entityIDQuestHolder);
    }
}
