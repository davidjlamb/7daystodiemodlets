﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Xml;
using HarmonyLib;
using UnityEngine;
/*
 * OLD! NOT USED!
 * 
/*
[HarmonyPatch(typeof(QuestActionSpawnGSEnemy))]
[HarmonyPatch("SpawnQuestEntity")]
public class ARS_QuestActionSpawnGSEnemy_SpawnQuestEntity {
    static bool Prefix(QuestActionSpawnGSEnemy __instance, ref int spawnedEntityID, ref int entityIDQuestHolder, ref EntityPlayer player) {
        World world = GameManager.Instance.World;
        if ((UnityEngine.Object)player == (UnityEngine.Object)null)
            player = world.GetEntity(entityIDQuestHolder) as EntityPlayer;
        Vector3 _transformPos;
        Vector3 _rotation = new Vector3(0.0f, player.transform.eulerAngles.y + 180f, 0.0f);
        bool foundPos = false;
        int tries = 20;
        do {
            Vector3 vector3 = new Vector3((float)((double)world.GetGameRandom().RandomFloat * 2.0 - 1.0), 0.0f, (float)((double)world.GetGameRandom().RandomFloat * 2.0 - 1.0));
            vector3.Normalize();
            float num = (float)((double)world.GetGameRandom().RandomFloat * 12.0 + 12.0);
            _transformPos = player.position + vector3 * num;
            float height = (float)GameManager.Instance.World.GetHeight((int)_transformPos.x, (int)_transformPos.z);
            float terrainHeight = (float)GameManager.Instance.World.GetTerrainHeight((int)_transformPos.x, (int)_transformPos.z);
            _transformPos.y = (float)(((double)height + (double)terrainHeight) / 2.0 + 1.5);
            foundPos = world.ChunkClusters[0].ChunkProvider.GetDynamicPrefabDecorator().GetPrefabFromWorldPosInsideWithOffset((int)_transformPos.x, (int)_transformPos.z, 2) != null;
            tries--;
        } while (!foundPos && tries > 0);
        if (foundPos) {
            Entity entity = EntityFactory.CreateEntity(spawnedEntityID, _transformPos, _rotation);
            entity.SetSpawnerSource(EnumSpawnerSource.Dynamic);
            GameManager.Instance.World.SpawnEntityInWorld(entity);
            (entity as EntityAlive).SetAttackTarget((EntityAlive)player, 200);
        }
        return false;
    }
}*/
