﻿using System;
using System.Runtime.CompilerServices;
using HarmonyLib;

[HarmonyPatch(typeof(EntityZombie))]
[HarmonyPatch("DamageEntity")]
public class ARSHeadshotDamage_EntityZombie_DamageEntity {
    [HarmonyReversePatch]
    [MethodImpl(MethodImplOptions.NoInlining)]
    public static int DamageEntity(EntityAlive instance, DamageSource _damageSource, int _strength, bool _criticalHit, float impulseScale) {
        throw new NotImplementedException("It's a stub");
    }

    static bool Prefix(EntityZombie __instance, ref DamageSource _damageSource, ref int _strength) {
        if (_strength > 999)
            return true;
        EnumBodyPartHit bodyPart = _damageSource.GetEntityDamageBodyPart(__instance);
        if (bodyPart == EnumBodyPartHit.Head) {
            if (_damageSource.damageType == EnumDamageTypes.Piercing) {
                _strength *= 3;
                _damageSource.DismemberChance = 0.6f;
            } else {
                _strength *= 2;
            }
            //_damageSource.DamageMultiplier = (float)(110f - (GamePrefs.GetInt(EnumGamePrefs.GameDifficulty) * 10f)) / 100f;
        } else {
            _strength = (int)(_strength * (float)(25f - (GamePrefs.GetInt(EnumGamePrefs.GameDifficulty) * 5f)) / 100f);
            //_damageSource.DamageMultiplier = (float)(25f - (GamePrefs.GetInt(EnumGamePrefs.GameDifficulty) * 5f)) / 100f;
        }
        return false;
    }
        
    static int Postfix(int __result, EntityZombie __instance, DamageSource _damageSource, int _strength, bool _criticalHit, float impulseScale) {
        return DamageEntity(__instance, _damageSource, _strength, _criticalHit, impulseScale);
    }
}
