﻿using HarmonyLib;

/*
[HarmonyPatch(typeof(WorldStaticData))]
[HarmonyPatch("InitSync")]
public class ARSDynamicContainerLootList_WorldStaticData_InitSync
{
    static bool Prefix()
    {
        WorldStaticData.xmlsToLoad.push(new WorldStaticData.XmlLoadInfo("dynamiccontainerloot", true, true, new Func<XmlFile, IEnumerator>(ARS_DynamicContainerLoot.CreateLists), new System.Action(ARS_DynamicContainerLoot.Cleanup)));
        return true;
    }
}
*/

[HarmonyPatch(typeof(BlockLoot))]
[HarmonyPatch("addTileEntity")]
public class ARSDynamicContainerLootList_BlockLoot_addTileEntity {
    static bool Prefix(BlockLoot __instance, ref WorldBase world, ref Chunk _chunk, ref Vector3i _blockPos, ref BlockValue _blockValue) {
        Vector3i blockPos = World.toBlock(_blockPos);
        string biomeName = ((World) world).Biomes.GetBiome(_chunk.GetBiomeId(blockPos.x, blockPos.z)).m_sBiomeName;
        string prefabName = ((World) world).ChunkClusters[0].ChunkProvider.GetDynamicPrefabDecorator().GetPrefabFromWorldPosInsideWithOffset((int)_blockPos.x, (int)_blockPos.z, 5)?.prefab?.FileNameNoExtension;
        string lootList = DynamicContainerLootList.GetLootList(__instance.GetBlockName(), prefabName, biomeName, __instance.Properties.Values["LootList"]);
        TileEntityLootContainer _te = new TileEntityLootContainer(_chunk);
        _te.localChunkPos = World.toBlock(_blockPos);
        _te.lootListName = lootList;
        _te.SetContainerSize(LootContainer.GetLootContainer(lootList).size);
        _chunk.AddTileEntity((TileEntity)_te);
        return false;
    }
}
