﻿using HarmonyLib;
using Microsoft.Win32;
using System;
using UnityEngine;

[HarmonyPatch(typeof(ItemActionRanged))]
[HarmonyPatch("ConsumeAmmo")]
public class ARSDropOnFire_ItemActionRanged_ConsumeAmmo {
    public static void Postfix(ItemActionRanged __instance, ItemActionData _actionData) {
        if (!(_actionData.invData.holdingEntity is EntityPlayerLocal))
            return;
        ItemValue itemValue = ItemClass.GetItem(__instance.MagazineItemNames[(int)_actionData.invData.itemValue.SelectedAmmoTypeIndex], false);
        if (!itemValue.ItemClass.Properties.Values.ContainsKey("DropOnFire"))
            return;
        EntityPlayerLocal entityPlayerLocal = _actionData.invData.holdingEntity as EntityPlayerLocal;
        ItemStack itemStack = new ItemStack(ItemClass.GetItem(itemValue.ItemClass.Properties.Values["DropOnFire"], false), 1);
        Vector3 pos = entityPlayerLocal.GetPosition();
        //TODO: Fix position based on player rotation
        //Vector3 _rotation = new Vector3(0.0f, player.transform.eulerAngles.y + 180f, 0.0f);
        pos.x += 0.5f;
        pos.z -= 0.2f;
        entityPlayerLocal.world.gameManager.ItemDropServer(itemStack, pos, Vector3.zero, -1, 60f, false);
    }
}
