﻿using HarmonyLib;
using Microsoft.Win32;

[HarmonyPatch(typeof(XUiC_OptionsVideo))]
[HarmonyPatch("Init")]
public class ARSDisableCrosshair_XUiC_OptionsVideo_Init
{
    static void Postfix(XUiC_OptionsVideo __instance)
    {
        XUiC_ComboBoxBool comboShowCrosshair = __instance.GetChildById("ShowCrosshair").GetChildByType<XUiC_ComboBoxBool>();
        comboShowCrosshair.OnValueChangedGeneric += new XUiC_ComboBox<bool>.XUiEvent_GenericValueChanged((XUiController _sender) => {
            RegistryKey r1 = Registry.CurrentUser.OpenSubKey("Software\\The Fun Pimps\\7 Days To Die", true);
            r1.SetValue("ShowCrosshair", comboShowCrosshair.Value == true ? "true" : "false");
            r1.Close();
            XUiC_SimpleButton btn = __instance.GetChildById("btnApply") as XUiC_SimpleButton;
            btn.Enabled = true;
        });
        RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\The Fun Pimps\\7 Days To Die", true);
        comboShowCrosshair.Value = (string)key.GetValue("ShowCrosshair", "true") == "true";
        key.Close();
    }
}

[HarmonyPatch(typeof(ItemClass))]
[HarmonyPatch("GetCrosshairType")]
public class ARSDisableCrosshair_ItemClass_GetCrosshairType
{
    static ItemClass.EnumCrosshairType Postfix(ItemClass.EnumCrosshairType __result, ItemClass __instance) {
        RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\The Fun Pimps\\7 Days To Die", false);
        bool show = (string)key.GetValue("ShowCrosshair", "true") == "true";
        key.Close();
        if (show == false)
            return ItemClass.EnumCrosshairType.None;
        else
            return __result;
    }
}
